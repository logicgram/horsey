'use strict';

import sum from 'hash-sum';
import sell from 'sell';
import emitter from 'contra/emitter';
import bullseye from 'bullseye';
import crossvent from 'crossvent';
import fuzzysort from 'fuzzysort';
import debounce from 'lodash/debounce';

const KEY_BACKSPACE = 8;
const KEY_ENTER = 13;
const KEY_ESC = 27;
const KEY_LEFT = 37;
const KEY_UP = 38;
const KEY_RIGHT = 39;
const KEY_DOWN = 40;
const KEY_TAB = 9;
const doc = document;
const docElement = doc.documentElement;

export function horsey(el, options = {}) {
  polyfills();
  const {
    setAppends,
    set,
    filterCategories,
    filterSuggestions,
    source,
    cache = {},
    predictNextSearch,
    renderItem,
    renderOverflow,
    renderCategory,
    blankSearch,
    appendTo,
    trackPosition,
    fixPosition,
    anchor,
    fuzzyKeys,
    debounce,
    additionalCacheAttributes,
  } = options;
  const caching = options.cache !== false;
  if (!source) {
    return;
  }

  const userGetText = options.getText;
  const userGetValue = options.getValue;
  const getText = (
    typeof userGetText === 'string' ? d => d[userGetText] :
      typeof userGetText === 'function' ? userGetText :
        d => d.toString()
  );
  const getValue = (
    typeof userGetValue === 'string' ? d => d[userGetValue] :
      typeof userGetValue === 'function' ? userGetValue :
        d => d
  );

  let previousSuggestions = [];
  let previousSelection = null;
  const limit = Number(options.limit) || Infinity;
  const overflowIncrement = Number(options.overflowIncrement) || null;
  const fuzzyThreshold = Number(options.fuzzyThreshold) || -Infinity;
  const completer = autocomplete(el, {
    source: sourceFunction,
    limit,
    overflowIncrement,
    fuzzyKeys,
    fuzzyThreshold,
    getText,
    getValue,
    setAppends,
    predictNextSearch,
    renderItem,
    renderOverflow,
    renderCategory,
    appendTo,
    trackPosition,
    fixPosition,
    anchor,
    noMatches,
    i18n: options.i18n,
    loadingSourcesDelay: options.loadingSourcesDelay,
    blankSearch,
    debounce,
    set(s) {
      if (setAppends !== true) {
        el.value = '';
      }
      previousSelection = s;
      (set || completer.defaultSetter)(getText(s), s);
      completer.emit('afterSet');
    },
    filterCategories,
    filterSuggestions,
    clearCache,
    getHash,
    getAdditionalCacheAttributes,
  });
  return completer;

  function noMatches(data) {
    if (!options.noMatches) {
      return false;
    }
    return data.query.length;
  }

  function getAdditionalCacheAttributes() {
    return additionalCacheAttributes && JSON.stringify(additionalCacheAttributes()) || '';
  }

  function getHash(query) {
    // fast, case insensitive, prevents collisions
    return sum([query, getAdditionalCacheAttributes()].join(''));
  }

  function sourceFunction(data, done, partial) {
    const {query, limit} = data;
    if (!options.blankSearch && query.length === 0) {
      done(null, [], true);
      return;
    }
    if (completer) {
      completer.emit('beforeUpdate');
    }
    const hash = getHash(query);
    if (caching) {
      const entry = cache[hash];
      if (entry) {
        const start = entry.created.getTime();
        const duration = cache.duration || 60 * 60 * 24;
        const diff = duration * 1000;
        const fresh = new Date(start + diff) > new Date();
        if (fresh) {
          done(null, entry.items.slice());
          return;
        }
      }
    }
    var sourceData = {
      previousSuggestions: previousSuggestions.slice(),
      previousSelection,
      input: query,
      renderItem,
      renderOverflow,
      renderCategory,
      limit
    };
    if (typeof options.source === 'function') {
      options.source(sourceData, sourced, partialSourced);
    } else {
      sourced(null, options.source);
    }

    function callback(err, result) {
      if (err) {
        console.log('Autocomplete source error.', err, el);
        done(err, []);
      }
      const items = Array.isArray(result) ? result : [];
      if (caching) {
        cache[hash] = {created: new Date(), items};
      }
      previousSuggestions = items;

      return items;
    }

    function sourced(err, result) {
      done(null, callback(err, result).slice());
    }

    function partialSourced(err, result) {
      partial(null, callback(err, result).slice());
    }
  }

  function clearCache() {
    Object.keys(cache).map((key) => delete cache[key]);
  }
}

function autocomplete(el, options = {}) {
  const o = options;
  const parent = o.appendTo || doc.body;
  const {
    getText,
    getValue,
    form,
    source,
    noMatches,
    i18n = {},
    highlighter = true,
    highlightCompleteWords = true,
    fixPosition = false,
    renderItem = defaultItemRenderer,
    renderOverflow = defaultOverflowRenderer,
    renderCategory = defaultCategoryRenderer,
    getHash,
    getAdditionalCacheAttributes,
    setAppends,
  } = o;
  const trackPosition = o.trackPosition || !fixPosition;
  const limit = typeof o.limit === 'number' ? o.limit : Infinity;
  const overflowIncrement = typeof o.overflowIncrement === 'number' ? o.overflowIncrement : null;
  const fuzzyKeys = o.fuzzyKeys || ['text'];
  const fuzzyThreshold = typeof o.fuzzyThreshold === 'number' ? o.fuzzyThreshold : -Infinity;
  const loadingSourcesDelay = typeof o.loadingSourcesDelay === 'number' ? o.loadingSourcesDelay : 500;
  const userFilterCategories = o.filterCategories || (() => true);
  const userFilterSuggestions = o.filterSuggestions || ((query, categoryData) => {
    const normalizedList = (categoryData.list || []).map(suggestion => fuzzyKeys.reduce((_normalized, key) => {
      _normalized[key] = stripAccents(suggestion[key]);

      return _normalized;
    }, {__original: suggestion}));

    const result = fuzzysort.go(
        stripAccents(query), normalizedList, {keys: fuzzyKeys, threshold: fuzzyThreshold}
      ).map(line => line.obj && line.obj.__original)
      .filter(res => !!res);

    return [result.slice(0, categoryData.limit || limit), result.length];
  });
  const userSet = o.set || defaultSetter;
  const categories = tag('div', 'sey-categories');
  const container = tag('div', 'sey-container' + (fixPosition ? ' sey-fixed' : ''));
  const deferredFiltering = defer(filtering);
  const state = {counter: 0, query: null, hash: null, additionalAttributes: null};
  const clearCache = () => {
    state.hash = state.additionalAttributes = null;
    o.clearCache();
  };
  let categoryMap = Object.create(null);
  let selection = null;
  let eye;
  let attachment = el;
  let noneMatch;
  let textInput;
  let anyInput;
  let ranchorleft;
  let ranchorright;
  let lastPrefix = '';
  const debounceTime = o.debounce || 300;
  const debouncedLoading = debounce(loading, debounceTime);

  if (o.autoHideOnBlur === void 0) {
    o.autoHideOnBlur = true;
  }
  if (o.autoHideOnClick === void 0) {
    o.autoHideOnClick = true;
  }
  if (o.autoShowOnUpDown === void 0) {
    o.autoShowOnUpDown = el.tagName === 'INPUT';
  }
  if (o.anchor) {
    ranchorleft = new RegExp('^' + o.anchor);
    ranchorright = new RegExp(o.anchor + '$');
  }

  let hasItems = false;
  const api = emitter({
    anchor: o.anchor,
    clear,
    show,
    hide,
    toggle,
    destroy,
    refreshPosition,
    appendText,
    appendHTML,
    filterAnchoredText,
    filterAnchoredHTML,
    defaultAppendText: appendText,
    defaultItemRenderer,
    defaultCategoryRenderer,
    defaultSetter,
    retarget,
    attachment,
    source: [],
    clearCache,
  });

  retarget(el);
  container.appendChild(categories);
  if (noMatches && i18n.noMatches) {
    noneMatch = tag('div', 'sey-empty sey-hide');
    text(noneMatch, i18n.noMatches);
    container.appendChild(noneMatch);
  }
  let $loadingSources, loadingSourcesTimeout;
  if (i18n.loadingSources) {
    $loadingSources = tag('div', 'sey-loading sey-hide');
    text($loadingSources, i18n.loadingSources);
    container.appendChild($loadingSources);
  }

  parent.appendChild(container);
  el.setAttribute('autocomplete', 'off');

  if (Array.isArray(source)) {
    loaded(source, false);
  }

  return api;

  function retarget(el) {
    inputEvents(true);
    attachment = api.attachment = el;
    textInput = attachment.tagName === 'INPUT' || attachment.tagName === 'TEXTAREA';
    anyInput = textInput || isEditable(attachment);
    inputEvents();
  }

  function refreshPosition() {
    if (eye) {
      eye.refresh();
    }
  }

  function loading(forceShow) {
    if (typeof source !== 'function') {
      return;
    }
    crossvent.remove(attachment, 'focus', loading);
    const query = readInput();
    const hash = getHash(query);
    if (hash === state.hash) {
      return;
    }
    hasItems = false;
    state.hash = hash;
    state.additionalAttributes = getAdditionalCacheAttributes();

    const counter = ++state.counter;


    if ($loadingSources) {
      if (loadingSourcesTimeout) {
        clearTimeout(loadingSourcesTimeout);
      }
      loadingSourcesTimeout = setTimeout(() => $loadingSources.classList.remove('sey-hide'), loadingSourcesDelay);
    }

    source({query, limit}, sourced, partialSourced);

    function sourced(err, result, blankQuery) {
      if ($loadingSources) {
        if (loadingSourcesTimeout) {
          clearTimeout(loadingSourcesTimeout);
          loadingSourcesTimeout = 0;
        }
        $loadingSources.classList.add('sey-hide');
      }

      if (state.counter !== counter) {
        return;
      }

      if (readInput()) {
        show();
      }

      loaded(result, forceShow);
      if (err || blankQuery) {
        hasItems = false;
      }
    }

    function partialSourced(err, result, blankQuery) {
      if (readInput()) {
        show();
      }

      loaded(result, forceShow);
      if (err || blankQuery) {
        hasItems = false;
      }
    }
  }

  function loaded(categories, forceShow) {
    clear();
    hasItems = true;
    api.source = [];

    if (forceShow) {
      show();
    }

    const input = readInput();
    categories.filter(category =>
      category.list.length && userFilterCategories(input, category, categories)
    ).map(categoryData => {
      const cat = getCategory(categoryData);
      cat.data.list.map(suggestion => add(suggestion, cat));
      crossvent.add(cat.ul, 'horsey-filter', filterCategory);

      function filterCategory() {
        const [results, total] = userFilterSuggestions(input, cat.data);

        // sort
        {
          const $fragment = document.createDocumentFragment();
          results.map(result => result.li).map($li => {
            $li.classList.remove('sey-hide');
            if (highlighter) {
              highlight($li, input);
            }

            $fragment.appendChild($li);
          });
          // hide remaining
          Array.from(cat.ul.childNodes).map($li => crossvent.fabricate($li, 'horsey-hide'));
          results.map(result => result.li).map($li => cat.ul.appendChild($li));


          const count = total - results.length;
          const $overflow = cat.ul.querySelector('.sey-item.set-item-overflow');
          if (!overflowIncrement || count < 1) {
            if ($overflow) {
              $overflow.classList.add('sey-hide');
            }
            return;
          }

          if (!$overflow) {
            addOverflow(count, cat);
            return;
          }

          $overflow.classList.remove('sey-hide');
          renderOverflow($overflow, count);

          // move back to the end
          $fragment.appendChild($overflow);
          cat.ul.appendChild($overflow);
        }
      }
    });

    filtering();
  }

  function clear() {
    unselect();
    while (categories.lastChild) {
      categories.removeChild(categories.lastChild);
    }
    categoryMap = Object.create(null);
    hasItems = false;
  }

  function stripAccents(str) {
    return (str || '').normalize('NFD').replace(/[\u0300-\u036f]/g, '');
  }

  function readInput() {
    return (textInput ? el.value : el.innerHTML).trim();
  }

  function getCategory(data) {
    if (!data.id) {
      data.id = 'default';
    }
    if (!categoryMap[data.id]) {
      categoryMap[data.id] = createCategory();
    }
    return categoryMap[data.id];

    function createCategory() {
      const category = tag('div', 'sey-category');
      const ul = tag('ul', 'sey-list');
      renderCategory(category, data);
      category.appendChild(ul);
      categories.appendChild(category);
      return {data, ul};
    }
  }

  function add(suggestion, cat) {
    const li = tag('li', 'sey-item');
    renderItem(li, suggestion);

    suggestion.li = li;

    crossvent.add(li, 'mouseenter', hoverSuggestion);
    crossvent.add(li, 'click', clickedSuggestion);
    crossvent.add(li, 'horsey-hide', hideItem);

    cat.ul.appendChild(li);

    const subitems = [];
    if (cat.data.submenu && cat.data.submenu.length) {
      const submenu = tag('span', 'sey-submenu-icon');
      crossvent.add(submenu, 'click', (e) => {
        const which = e.which || e.keyCode;

        if (!submenu.classList.contains('deployed') && which !== KEY_LEFT) {
          deployedSuggestion(e);
          submenu.classList.add('deployed');
        } else if (which !== KEY_RIGHT) {
          collapsedSuggestion(e);
          submenu.classList.remove('deployed');
        }
      });

      li.appendChild(submenu);
    }


    api.source.push(suggestion);
    return li;

    function hoverSuggestion() {
      select(li);
    }

    function clickedSuggestion(e, selectedSuggestion) {
      if (selectedSuggestion) {
        selectedSuggestion.parent = suggestion;
      }

      selectedSuggestion = selectedSuggestion || suggestion;
      const input = getText(selectedSuggestion);
      set(selectedSuggestion);
      hide();
      attachment.focus();
      lastPrefix = o.predictNextSearch && o.predictNextSearch({
        input: input,
        source: api.source.slice(),
        selection: selectedSuggestion
      }) || '';
      if (lastPrefix) {
        el.value = lastPrefix;
        el.select();
        show();
        filtering();
      }
    }

    function deployedSuggestion(e) {
      stop(e);

      subitems.push(...[...cat.data.submenu].reverse().map(subSuggestion => {
        const subitem = tag('li', 'sey-item sey-sub-item');
        renderItem(subitem, subSuggestion);

        crossvent.add(subitem, 'click', (e) => clickedSuggestion(e, subSuggestion));

        li.after(subitem);
        return subitem;
      }));

      select(subitems[subitems.length - 1]);

      return false;
    }

    function collapsedSuggestion(e) {
      stop(e);

      subitems.map(subitem => cat.ul.removeChild(subitem));
      subitems.length = 0;

      select(li);

      return false;
    }

    function hideItem() {
      if (!hidden(li)) {
        li.classList.add('sey-hide');
        if (selection === li) {
          unselect();
        }
      }
    }
  }

  function addOverflow(count, cat) {
    const $li = tag('li', 'sey-item set-item-overflow');
    renderOverflow($li, count);

    crossvent.add($li, 'mouseenter', hoverOverflow);
    crossvent.add($li, 'click', clickedOverflow);
    cat.ul.appendChild($li);
    return $li;

    function hoverOverflow() {
      select($li);
    }

    function clickedOverflow() {
      cat.data.limit += overflowIncrement;

      // fixme kinda ungly
      const $previous = $li.previousSibling;
      filtering();
      select($previous.nextSibling);
    }
  }

  function highlight($el, needle) {
    $el = $el.querySelector('.sey-item-text') || $el;
    // FIXME highlight stripAccents
    $el.innerHTML = fuzzysort.highlight(
      fuzzysort.single(needle, $el.innerText), '<span class="sey-highlight">', '</span>'
    ) || $el.innerText;
  }

  function set(value) {
    if (o.anchor) {
      return (isText() ? api.appendText : api.appendHTML)(getValue(value));
    }
    userSet(value);
  }

  function isText() {
    return isInput(attachment);
  }

  function visible() {
    return container.classList.contains('sey-show');
  }

  function hidden(li) {
    return li.classList.contains('sey-hide');
  }

  function show() {
    if (eye) {
      eye.refresh();
    }

    setFixedPosition();

    if (!visible()) {
      container.classList.add('sey-show');
      crossvent.fabricate(attachment, 'horsey-show');
    }
  }

  function setFixedPosition() {
    if (!fixPosition || !attachment) {
      return;
    }

    requestAnimationFrame(() => {
      const {bottom, left, right} = el.getBoundingClientRect();
      Object.assign(container.style, {
        top: `${bottom}px`,
        left: `${left}px`,
        width: `${right - left}px`,
        maxHeight: `calc(100vh - ${bottom}px - 15px)`
      });
    });
  }

  function toggler(e) {
    const left = e.which === 1 && !e.metaKey && !e.ctrlKey;
    if (left === false) {
      return; // we only care about honest to god left-clicks
    }
    toggle();
  }

  function toggle() {
    if (!visible()) {
      show();
    } else {
      hide();
    }
  }

  function select(li) {
    unselect();

    if (li.classList.contains('sey-hide')) {
      selection = null;
      return;
    }

    if (li) {
      selection = li;
      selection.classList.add('sey-selected');
    }

    // scroll to category if first visible item
    const selectionTop = Array.from(
      selection.closest('.sey-list').querySelectorAll('.sey-item:not(.sey-hide)')
    ).indexOf(selection) === 0 ? selection.closest('.sey-category').querySelector('.sey-category-id') : selection;

    // if no category
    if (!selectionTop) {
      return;
    }

    const offsetTop = selectionTop.offsetTop;
    const offsetBottom = selection.offsetTop + selection.offsetHeight;

    const visibleTop = container.scrollTop;
    const visibleBottom = visibleTop + container.offsetHeight;

    if (offsetTop < visibleTop) {
      container.scrollTop = offsetTop;
    } else if (offsetBottom > visibleBottom) {
      container.scrollTop = offsetBottom - container.offsetHeight;
    }
  }

  function unselect() {
    if (selection) {
      selection.classList.remove('sey-selected');
      selection = null;
    }
  }

  function move(up, moveCategory) {
    const total = api.source.length;
    if (total === 0) {
      return;
    }

    const first = up ? 'lastChild' : 'firstChild';
    const last = up ? 'firstChild' : 'lastChild';
    const next = up ? 'previousSibling' : 'nextSibling';

    let liIterator = selection || findList(categories[first])[first];
    let li = liIterator;

    let categoryLast;
    if (moveCategory) {
      const visibleSiblings = Array.from(liIterator.closest('.sey-list').querySelectorAll('.sey-item:not(.sey-hide)'));

      categoryLast = findCategory(li);
      // last of categoryLast, jump to next
      if (visibleSiblings.indexOf(li) === (up ? 0 : visibleSiblings.length - 1)) {
        let categoryIterator = categoryLast;
        while (categoryIterator[next]) {
          categoryIterator = categoryIterator[next];

          if (!hidden(categoryIterator)) {
            categoryLast = categoryIterator;
            break;
          }
        }
      }
    } else {
      categoryLast = categories[last];
    }

    const liLast = findList(categoryLast)[last];
    while (liIterator !== liLast) {
      let next = findNext(liIterator);
      if (typeof up === 'undefined' && liIterator === li) {
        up = false;
        next = liIterator;
      }

      if (!next) {
        break;
      }

      liIterator = next;
      if (hidden(liIterator)) {
        continue;
      }

      li = liIterator;
      if (!moveCategory) {
        break;
      }
    }

    select(li || liIterator);

    function findCategory(el) {
      while (el) {
        if (!el.parentElement) {
          console.log(el);
        }
        if (el.parentElement.classList.contains('sey-category')) {
          return el.parentElement;
        }
        el = el.parentElement;
      }
      return null;
    }

    function findNext(li) {
      if (!li) {
        return;
      }

      if (li[next]) {
        return li[next];
      }

      const category = findCategory(liIterator);
      if (category[next] && findList(category[next])[first]) {
        return findList(category[next])[first];
      }
    }
  }

  function hide() {
    if (eye) {
      eye.sleep();
    }
    container.classList.remove('sey-show');
    unselect();
    crossvent.fabricate(attachment, 'horsey-hide');
    if (el.value === lastPrefix) {
      el.value = '';
    }
  }

  function keydown(e) {
    const shown = visible();
    const which = e.which || e.keyCode;
    if (which === KEY_DOWN) {
      if (anyInput && o.autoShowOnUpDown) {
        show();
      }
      if (shown) {
        move(false, e.shiftKey);
        stop(e);
      }
    } else if (which === KEY_UP) {
      if (anyInput && o.autoShowOnUpDown) {
        show();
      }
      if (shown) {
        move(true, e.shiftKey);
        stop(e);
      }
    } else if (which === KEY_RIGHT || which === KEY_LEFT) {
      while (selection && selection.classList.contains('sey-sub-item')) {
        selection = selection.previousElementSibling;
      }

      const $submenu = selection && selection.querySelector('.sey-submenu-icon');
      if ($submenu) {
        crossvent.fabricate($submenu, 'click');
      }
    } else if (which === KEY_BACKSPACE) {
      if (anyInput && o.autoShowOnUpDown) {
        show();
      }
    } else if (shown) {
      if (which === KEY_ENTER) {
        if (selection) {
          crossvent.fabricate(selection, 'click');
        } else {
          hide();
        }
        stop(e);
      } else if (which === KEY_ESC) {
        hide();
        stop(e);
      }
    }
  }

  function stop(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  function showNoResults() {
    if (noneMatch) {
      noneMatch.classList.remove('sey-hide');
    }
  }

  function hideNoResults() {
    if (noneMatch) {
      noneMatch.classList.add('sey-hide');
    }
  }

  function filtering() {
    if (!visible()) {
      return;
    }
    debouncedLoading(true);
    crossvent.fabricate(attachment, 'horsey-filter');
    const value = readInput();
    if ((!o.blankSearch && !value) || state.additionalAttributes !== getAdditionalCacheAttributes()) {
      hide();
      return;
    }
    const nomatch = noMatches({query: value});
    let count = walkCategories();
    if (count === 0 && nomatch && hasItems) {
      showNoResults();
    } else {
      hideNoResults();
    }
    if (!selection) {
      move();
    }
    if (!selection && !nomatch) {
      hide();
    }

    function walkCategories() {
      return Array.from(categories.childNodes).reduce((count, category) => {
        const list = findList(category);
        const partial = walkCategory(list);
        category.classList[partial ? 'remove' : 'add']('sey-hide');

        count += partial;
        return count;
      }, 0);
    }

    function walkCategory(ul) {
      crossvent.fabricate(ul, 'horsey-filter');
      return ul.querySelectorAll('li:not(.sey-hide)').length;
    }
  }

  function deferredFilteringNoEnter(e) {
    const which = e.which || e.keyCode;
    if (which === KEY_ENTER) {
      return;
    }
    deferredFiltering();
  }

  function deferredShow(e) {
    const which = e.which || e.keyCode;
    if (which === KEY_ENTER || which === KEY_TAB) {
      return;
    }
    setTimeout(show, 0);
  }

  function autocompleteEventTarget(e) {
    let target = e.target;
    if (target === attachment) {
      return true;
    }
    while (target) {
      if (target === container || target === attachment) {
        return true;
      }
      target = target.parentNode;
    }
  }

  function hideOnBlur(e) {
    const which = e.which || e.keyCode;
    if (which === KEY_TAB) {
      hide();
    }
  }

  function hideOnClick(e) {
    if (autocompleteEventTarget(e)) {
      return;
    }
    hide();
  }

  function inputEvents(remove) {
    const op = remove ? 'remove' : 'add';
    if (eye) {
      eye.destroy();
      eye = null;
    }
    if (trackPosition && !remove) {
      eye = bullseye(container, attachment, {
        caret: anyInput && attachment.tagName !== 'INPUT',
        context: o.appendTo
      });
      if (!visible()) {
        eye.sleep();
      }
    }
    if (remove || (anyInput && doc.activeElement !== attachment)) {
      crossvent[op](attachment, 'focus', loading);
    } else {
      loading();
    }
    if (anyInput) {
      crossvent[op](attachment, 'input', deferredShow);
      crossvent[op](attachment, 'input', deferredFiltering);
      crossvent[op](attachment, 'input', deferredFilteringNoEnter);
      /*crossvent[op](attachment, 'paste', deferredFiltering);*/
      crossvent[op](attachment, 'keydown', keydown);
      if (o.autoHideOnBlur) {
        crossvent[op](attachment, 'input', hideOnBlur);
      }
    } else {
      crossvent[op](attachment, 'click', toggler);
      crossvent[op](docElement, 'keydown', keydown);
    }
    if (o.autoHideOnClick) {
      crossvent[op](doc, 'click', hideOnClick);
    }
    if (fixPosition) {
      window.addEventListener('resize', setFixedPosition);
    }
    if (form) {
      crossvent[op](form, 'submit', hide);
    }
  }

  function destroy() {
    inputEvents(true);
    if (parent.contains(container)) {
      parent.removeChild(container);
    }
  }

  function defaultSetter(value) {
    if (textInput) {
      if (setAppends === true) {
        el.value += ' ' + value;
      } else {
        el.value = value;
      }
    } else {
      if (setAppends === true) {
        el.innerHTML += ' ' + value;
      } else {
        el.innerHTML = value;
      }
    }

    el.dispatchEvent(new Event('change'));
    el.dispatchEvent(new Event('input'));
  }

  function defaultItemRenderer(li, suggestion) {
    text(li, getText(suggestion));
  }

  function defaultOverflowRenderer(li, count) {
    text(li, (i18n.more || '... ##count## more').replace(/##count##/g, count));
  }

  function defaultCategoryRenderer(div, data) {
    if (data.id !== 'default') {
      const id = tag('div', 'sey-category-id');
      div.appendChild(id);
      text(id, data.id);
    }
  }

  function loopbackToAnchor(text, p) {
    let result = '';
    let anchored = false;
    let start = p.start;
    while (anchored === false && start >= 0) {
      result = text.substr(start - 1, p.start - start + 1);
      anchored = ranchorleft.test(result);
      start--;
    }
    return {
      text: anchored ? result : null,
      start
    };
  }

  function filterAnchoredText(q, suggestion) {
    const position = sell(el);
    const input = loopbackToAnchor(q, position).text;
    if (input) {
      return {input, suggestion};
    }
  }

  function appendText(value) {
    const current = el.value;
    const position = sell(el);
    const input = loopbackToAnchor(current, position);
    const left = current.substr(0, input.start);
    const right = current.substr(input.start + input.text.length + (position.end - position.start));
    const before = left + value + ' ';

    el.value = before + right;
    sell(el, {start: before.length, end: before.length});
  }

  function filterAnchoredHTML() {
    throw new Error('Anchoring in editable elements is disabled by default.');
  }

  function appendHTML() {
    throw new Error('Anchoring in editable elements is disabled by default.');
  }

  function findList(category) {
    return category.querySelector('.sey-list');
  }
}

function isInput(el) {
  return el.tagName === 'INPUT' || el.tagName === 'TEXTAREA';
}

function tag(type, className) {
  const el = doc.createElement(type);
  el.className = className;
  return el;
}

function defer(fn) {
  return function () {
    setTimeout(fn, 0);
  };
}

function text(el, value) {
  el.innerText = el.textContent = value;
}

function isEditable(el) {
  const value = el.getAttribute('contentEditable');
  if (value === 'false') {
    return false;
  }
  if (value === 'true') {
    return true;
  }
  if (el.parentElement) {
    return isEditable(el.parentElement);
  }
  return false;
}

function polyfills() {
  //from: https://github.com/jserz/js_piece/blob/master/DOM/ChildNode/after()/after().md
  (function (arr) {
    arr.forEach(function (item) {
      if (item.hasOwnProperty('after')) {
        return;
      }
      Object.defineProperty(item, 'after', {
        configurable: true,
        enumerable: true,
        writable: true,
        value: function after() {
          var argArr = Array.prototype.slice.call(arguments),
            docFrag = document.createDocumentFragment();

          argArr.forEach(function (argItem) {
            var isNode = argItem instanceof Node;
            docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
          });

          this.parentNode.insertBefore(docFrag, this.nextSibling);
        }
      });
    });
  })([Element.prototype/*, CharacterData.prototype, DocumentType.prototype*/]);
}
