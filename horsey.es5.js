'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.horsey = horsey;

var _hashSum = require('hash-sum');

var _hashSum2 = _interopRequireDefault(_hashSum);

var _sell = require('sell');

var _sell2 = _interopRequireDefault(_sell);

var _emitter = require('contra/emitter');

var _emitter2 = _interopRequireDefault(_emitter);

var _bullseye = require('bullseye');

var _bullseye2 = _interopRequireDefault(_bullseye);

var _crossvent = require('crossvent');

var _crossvent2 = _interopRequireDefault(_crossvent);

var _fuzzysort = require('fuzzysort');

var _fuzzysort2 = _interopRequireDefault(_fuzzysort);

var _debounce = require('lodash/debounce');

var _debounce2 = _interopRequireDefault(_debounce);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var KEY_BACKSPACE = 8;
var KEY_ENTER = 13;
var KEY_ESC = 27;
var KEY_LEFT = 37;
var KEY_UP = 38;
var KEY_RIGHT = 39;
var KEY_DOWN = 40;
var KEY_TAB = 9;
var doc = document;
var docElement = doc.documentElement;

function horsey(el) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  polyfills();
  var setAppends = options.setAppends,
      _set = options.set,
      filterCategories = options.filterCategories,
      filterSuggestions = options.filterSuggestions,
      source = options.source,
      _options$cache = options.cache,
      cache = _options$cache === undefined ? {} : _options$cache,
      predictNextSearch = options.predictNextSearch,
      renderItem = options.renderItem,
      renderOverflow = options.renderOverflow,
      renderCategory = options.renderCategory,
      blankSearch = options.blankSearch,
      appendTo = options.appendTo,
      trackPosition = options.trackPosition,
      fixPosition = options.fixPosition,
      anchor = options.anchor,
      fuzzyKeys = options.fuzzyKeys,
      debounce = options.debounce,
      additionalCacheAttributes = options.additionalCacheAttributes;

  var caching = options.cache !== false;
  if (!source) {
    return;
  }

  var userGetText = options.getText;
  var userGetValue = options.getValue;
  var getText = typeof userGetText === 'string' ? function (d) {
    return d[userGetText];
  } : typeof userGetText === 'function' ? userGetText : function (d) {
    return d.toString();
  };
  var getValue = typeof userGetValue === 'string' ? function (d) {
    return d[userGetValue];
  } : typeof userGetValue === 'function' ? userGetValue : function (d) {
    return d;
  };

  var previousSuggestions = [];
  var previousSelection = null;
  var limit = Number(options.limit) || Infinity;
  var overflowIncrement = Number(options.overflowIncrement) || null;
  var fuzzyThreshold = Number(options.fuzzyThreshold) || -Infinity;
  var completer = autocomplete(el, {
    source: sourceFunction,
    limit: limit,
    overflowIncrement: overflowIncrement,
    fuzzyKeys: fuzzyKeys,
    fuzzyThreshold: fuzzyThreshold,
    getText: getText,
    getValue: getValue,
    setAppends: setAppends,
    predictNextSearch: predictNextSearch,
    renderItem: renderItem,
    renderOverflow: renderOverflow,
    renderCategory: renderCategory,
    appendTo: appendTo,
    trackPosition: trackPosition,
    fixPosition: fixPosition,
    anchor: anchor,
    noMatches: noMatches,
    i18n: options.i18n,
    loadingSourcesDelay: options.loadingSourcesDelay,
    blankSearch: blankSearch,
    debounce: debounce,
    set: function set(s) {
      if (setAppends !== true) {
        el.value = '';
      }
      previousSelection = s;
      (_set || completer.defaultSetter)(getText(s), s);
      completer.emit('afterSet');
    },

    filterCategories: filterCategories,
    filterSuggestions: filterSuggestions,
    clearCache: clearCache,
    getHash: getHash,
    getAdditionalCacheAttributes: getAdditionalCacheAttributes
  });
  return completer;

  function noMatches(data) {
    if (!options.noMatches) {
      return false;
    }
    return data.query.length;
  }

  function getAdditionalCacheAttributes() {
    return additionalCacheAttributes && JSON.stringify(additionalCacheAttributes()) || '';
  }

  function getHash(query) {
    // fast, case insensitive, prevents collisions
    return (0, _hashSum2.default)([query, getAdditionalCacheAttributes()].join(''));
  }

  function sourceFunction(data, done, partial) {
    var query = data.query,
        limit = data.limit;

    if (!options.blankSearch && query.length === 0) {
      done(null, [], true);
      return;
    }
    if (completer) {
      completer.emit('beforeUpdate');
    }
    var hash = getHash(query);
    if (caching) {
      var entry = cache[hash];
      if (entry) {
        var start = entry.created.getTime();
        var duration = cache.duration || 60 * 60 * 24;
        var diff = duration * 1000;
        var fresh = new Date(start + diff) > new Date();
        if (fresh) {
          done(null, entry.items.slice());
          return;
        }
      }
    }
    var sourceData = {
      previousSuggestions: previousSuggestions.slice(),
      previousSelection: previousSelection,
      input: query,
      renderItem: renderItem,
      renderOverflow: renderOverflow,
      renderCategory: renderCategory,
      limit: limit
    };
    if (typeof options.source === 'function') {
      options.source(sourceData, sourced, partialSourced);
    } else {
      sourced(null, options.source);
    }

    function callback(err, result) {
      if (err) {
        console.log('Autocomplete source error.', err, el);
        done(err, []);
      }
      var items = Array.isArray(result) ? result : [];
      if (caching) {
        cache[hash] = { created: new Date(), items: items };
      }
      previousSuggestions = items;

      return items;
    }

    function sourced(err, result) {
      done(null, callback(err, result).slice());
    }

    function partialSourced(err, result) {
      partial(null, callback(err, result).slice());
    }
  }

  function clearCache() {
    Object.keys(cache).map(function (key) {
      return delete cache[key];
    });
  }
}

function autocomplete(el) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var o = options;
  var parent = o.appendTo || doc.body;
  var getText = o.getText,
      getValue = o.getValue,
      form = o.form,
      source = o.source,
      noMatches = o.noMatches,
      _o$i18n = o.i18n,
      i18n = _o$i18n === undefined ? {} : _o$i18n,
      _o$highlighter = o.highlighter,
      highlighter = _o$highlighter === undefined ? true : _o$highlighter,
      _o$highlightCompleteW = o.highlightCompleteWords,
      highlightCompleteWords = _o$highlightCompleteW === undefined ? true : _o$highlightCompleteW,
      _o$fixPosition = o.fixPosition,
      fixPosition = _o$fixPosition === undefined ? false : _o$fixPosition,
      _o$renderItem = o.renderItem,
      renderItem = _o$renderItem === undefined ? defaultItemRenderer : _o$renderItem,
      _o$renderOverflow = o.renderOverflow,
      renderOverflow = _o$renderOverflow === undefined ? defaultOverflowRenderer : _o$renderOverflow,
      _o$renderCategory = o.renderCategory,
      renderCategory = _o$renderCategory === undefined ? defaultCategoryRenderer : _o$renderCategory,
      getHash = o.getHash,
      getAdditionalCacheAttributes = o.getAdditionalCacheAttributes,
      setAppends = o.setAppends;

  var trackPosition = o.trackPosition || !fixPosition;
  var limit = typeof o.limit === 'number' ? o.limit : Infinity;
  var overflowIncrement = typeof o.overflowIncrement === 'number' ? o.overflowIncrement : null;
  var fuzzyKeys = o.fuzzyKeys || ['text'];
  var fuzzyThreshold = typeof o.fuzzyThreshold === 'number' ? o.fuzzyThreshold : -Infinity;
  var loadingSourcesDelay = typeof o.loadingSourcesDelay === 'number' ? o.loadingSourcesDelay : 500;
  var userFilterCategories = o.filterCategories || function () {
    return true;
  };
  var userFilterSuggestions = o.filterSuggestions || function (query, categoryData) {
    var normalizedList = (categoryData.list || []).map(function (suggestion) {
      return fuzzyKeys.reduce(function (_normalized, key) {
        _normalized[key] = stripAccents(suggestion[key]);

        return _normalized;
      }, { __original: suggestion });
    });

    var result = _fuzzysort2.default.go(stripAccents(query), normalizedList, { keys: fuzzyKeys, threshold: fuzzyThreshold }).map(function (line) {
      return line.obj && line.obj.__original;
    }).filter(function (res) {
      return !!res;
    });

    return [result.slice(0, categoryData.limit || limit), result.length];
  };
  var userSet = o.set || defaultSetter;
  var categories = tag('div', 'sey-categories');
  var container = tag('div', 'sey-container' + (fixPosition ? ' sey-fixed' : ''));
  var deferredFiltering = defer(filtering);
  var state = { counter: 0, query: null, hash: null, additionalAttributes: null };
  var clearCache = function clearCache() {
    state.hash = state.additionalAttributes = null;
    o.clearCache();
  };
  var categoryMap = Object.create(null);
  var selection = null;
  var eye = void 0;
  var attachment = el;
  var noneMatch = void 0;
  var textInput = void 0;
  var anyInput = void 0;
  var ranchorleft = void 0;
  var ranchorright = void 0;
  var lastPrefix = '';
  var debounceTime = o.debounce || 300;
  var debouncedLoading = (0, _debounce2.default)(loading, debounceTime);

  if (o.autoHideOnBlur === void 0) {
    o.autoHideOnBlur = true;
  }
  if (o.autoHideOnClick === void 0) {
    o.autoHideOnClick = true;
  }
  if (o.autoShowOnUpDown === void 0) {
    o.autoShowOnUpDown = el.tagName === 'INPUT';
  }
  if (o.anchor) {
    ranchorleft = new RegExp('^' + o.anchor);
    ranchorright = new RegExp(o.anchor + '$');
  }

  var hasItems = false;
  var api = (0, _emitter2.default)({
    anchor: o.anchor,
    clear: clear,
    show: show,
    hide: hide,
    toggle: toggle,
    destroy: destroy,
    refreshPosition: refreshPosition,
    appendText: appendText,
    appendHTML: appendHTML,
    filterAnchoredText: filterAnchoredText,
    filterAnchoredHTML: filterAnchoredHTML,
    defaultAppendText: appendText,
    defaultItemRenderer: defaultItemRenderer,
    defaultCategoryRenderer: defaultCategoryRenderer,
    defaultSetter: defaultSetter,
    retarget: retarget,
    attachment: attachment,
    source: [],
    clearCache: clearCache
  });

  retarget(el);
  container.appendChild(categories);
  if (noMatches && i18n.noMatches) {
    noneMatch = tag('div', 'sey-empty sey-hide');
    text(noneMatch, i18n.noMatches);
    container.appendChild(noneMatch);
  }
  var $loadingSources = void 0,
      loadingSourcesTimeout = void 0;
  if (i18n.loadingSources) {
    $loadingSources = tag('div', 'sey-loading sey-hide');
    text($loadingSources, i18n.loadingSources);
    container.appendChild($loadingSources);
  }

  parent.appendChild(container);
  el.setAttribute('autocomplete', 'off');

  if (Array.isArray(source)) {
    loaded(source, false);
  }

  return api;

  function retarget(el) {
    inputEvents(true);
    attachment = api.attachment = el;
    textInput = attachment.tagName === 'INPUT' || attachment.tagName === 'TEXTAREA';
    anyInput = textInput || isEditable(attachment);
    inputEvents();
  }

  function refreshPosition() {
    if (eye) {
      eye.refresh();
    }
  }

  function loading(forceShow) {
    if (typeof source !== 'function') {
      return;
    }
    _crossvent2.default.remove(attachment, 'focus', loading);
    var query = readInput();
    var hash = getHash(query);
    if (hash === state.hash) {
      return;
    }
    hasItems = false;
    state.hash = hash;
    state.additionalAttributes = getAdditionalCacheAttributes();

    var counter = ++state.counter;

    if ($loadingSources) {
      if (loadingSourcesTimeout) {
        clearTimeout(loadingSourcesTimeout);
      }
      loadingSourcesTimeout = setTimeout(function () {
        return $loadingSources.classList.remove('sey-hide');
      }, loadingSourcesDelay);
    }

    source({ query: query, limit: limit }, sourced, partialSourced);

    function sourced(err, result, blankQuery) {
      if ($loadingSources) {
        if (loadingSourcesTimeout) {
          clearTimeout(loadingSourcesTimeout);
          loadingSourcesTimeout = 0;
        }
        $loadingSources.classList.add('sey-hide');
      }

      if (state.counter !== counter) {
        return;
      }

      if (readInput()) {
        show();
      }

      loaded(result, forceShow);
      if (err || blankQuery) {
        hasItems = false;
      }
    }

    function partialSourced(err, result, blankQuery) {
      if (readInput()) {
        show();
      }

      loaded(result, forceShow);
      if (err || blankQuery) {
        hasItems = false;
      }
    }
  }

  function loaded(categories, forceShow) {
    clear();
    hasItems = true;
    api.source = [];

    if (forceShow) {
      show();
    }

    var input = readInput();
    categories.filter(function (category) {
      return category.list.length && userFilterCategories(input, category, categories);
    }).map(function (categoryData) {
      var cat = getCategory(categoryData);
      cat.data.list.map(function (suggestion) {
        return add(suggestion, cat);
      });
      _crossvent2.default.add(cat.ul, 'horsey-filter', filterCategory);

      function filterCategory() {
        var _userFilterSuggestion = userFilterSuggestions(input, cat.data),
            _userFilterSuggestion2 = _slicedToArray(_userFilterSuggestion, 2),
            results = _userFilterSuggestion2[0],
            total = _userFilterSuggestion2[1];

        // sort


        {
          var $fragment = document.createDocumentFragment();
          results.map(function (result) {
            return result.li;
          }).map(function ($li) {
            $li.classList.remove('sey-hide');
            if (highlighter) {
              highlight($li, input);
            }

            $fragment.appendChild($li);
          });
          // hide remaining
          Array.from(cat.ul.childNodes).map(function ($li) {
            return _crossvent2.default.fabricate($li, 'horsey-hide');
          });
          results.map(function (result) {
            return result.li;
          }).map(function ($li) {
            return cat.ul.appendChild($li);
          });

          var count = total - results.length;
          var $overflow = cat.ul.querySelector('.sey-item.set-item-overflow');
          if (!overflowIncrement || count < 1) {
            if ($overflow) {
              $overflow.classList.add('sey-hide');
            }
            return;
          }

          if (!$overflow) {
            addOverflow(count, cat);
            return;
          }

          $overflow.classList.remove('sey-hide');
          renderOverflow($overflow, count);

          // move back to the end
          $fragment.appendChild($overflow);
          cat.ul.appendChild($overflow);
        }
      }
    });

    filtering();
  }

  function clear() {
    unselect();
    while (categories.lastChild) {
      categories.removeChild(categories.lastChild);
    }
    categoryMap = Object.create(null);
    hasItems = false;
  }

  function stripAccents(str) {
    return (str || '').normalize('NFD').replace(/[\u0300-\u036f]/g, '');
  }

  function readInput() {
    return (textInput ? el.value : el.innerHTML).trim();
  }

  function getCategory(data) {
    if (!data.id) {
      data.id = 'default';
    }
    if (!categoryMap[data.id]) {
      categoryMap[data.id] = createCategory();
    }
    return categoryMap[data.id];

    function createCategory() {
      var category = tag('div', 'sey-category');
      var ul = tag('ul', 'sey-list');
      renderCategory(category, data);
      category.appendChild(ul);
      categories.appendChild(category);
      return { data: data, ul: ul };
    }
  }

  function add(suggestion, cat) {
    var li = tag('li', 'sey-item');
    renderItem(li, suggestion);

    suggestion.li = li;

    _crossvent2.default.add(li, 'mouseenter', hoverSuggestion);
    _crossvent2.default.add(li, 'click', clickedSuggestion);
    _crossvent2.default.add(li, 'horsey-hide', hideItem);

    cat.ul.appendChild(li);

    var subitems = [];
    if (cat.data.submenu && cat.data.submenu.length) {
      var submenu = tag('span', 'sey-submenu-icon');
      _crossvent2.default.add(submenu, 'click', function (e) {
        var which = e.which || e.keyCode;

        if (!submenu.classList.contains('deployed') && which !== KEY_LEFT) {
          deployedSuggestion(e);
          submenu.classList.add('deployed');
        } else if (which !== KEY_RIGHT) {
          collapsedSuggestion(e);
          submenu.classList.remove('deployed');
        }
      });

      li.appendChild(submenu);
    }

    api.source.push(suggestion);
    return li;

    function hoverSuggestion() {
      select(li);
    }

    function clickedSuggestion(e, selectedSuggestion) {
      if (selectedSuggestion) {
        selectedSuggestion.parent = suggestion;
      }

      selectedSuggestion = selectedSuggestion || suggestion;
      var input = getText(selectedSuggestion);
      set(selectedSuggestion);
      hide();
      attachment.focus();
      lastPrefix = o.predictNextSearch && o.predictNextSearch({
        input: input,
        source: api.source.slice(),
        selection: selectedSuggestion
      }) || '';
      if (lastPrefix) {
        el.value = lastPrefix;
        el.select();
        show();
        filtering();
      }
    }

    function deployedSuggestion(e) {
      stop(e);

      subitems.push.apply(subitems, _toConsumableArray([].concat(_toConsumableArray(cat.data.submenu)).reverse().map(function (subSuggestion) {
        var subitem = tag('li', 'sey-item sey-sub-item');
        renderItem(subitem, subSuggestion);

        _crossvent2.default.add(subitem, 'click', function (e) {
          return clickedSuggestion(e, subSuggestion);
        });

        li.after(subitem);
        return subitem;
      })));

      select(subitems[subitems.length - 1]);

      return false;
    }

    function collapsedSuggestion(e) {
      stop(e);

      subitems.map(function (subitem) {
        return cat.ul.removeChild(subitem);
      });
      subitems.length = 0;

      select(li);

      return false;
    }

    function hideItem() {
      if (!hidden(li)) {
        li.classList.add('sey-hide');
        if (selection === li) {
          unselect();
        }
      }
    }
  }

  function addOverflow(count, cat) {
    var $li = tag('li', 'sey-item set-item-overflow');
    renderOverflow($li, count);

    _crossvent2.default.add($li, 'mouseenter', hoverOverflow);
    _crossvent2.default.add($li, 'click', clickedOverflow);
    cat.ul.appendChild($li);
    return $li;

    function hoverOverflow() {
      select($li);
    }

    function clickedOverflow() {
      cat.data.limit += overflowIncrement;

      // fixme kinda ungly
      var $previous = $li.previousSibling;
      filtering();
      select($previous.nextSibling);
    }
  }

  function highlight($el, needle) {
    $el = $el.querySelector('.sey-item-text') || $el;
    // FIXME highlight stripAccents
    $el.innerHTML = _fuzzysort2.default.highlight(_fuzzysort2.default.single(needle, $el.innerText), '<span class="sey-highlight">', '</span>') || $el.innerText;
  }

  function set(value) {
    if (o.anchor) {
      return (isText() ? api.appendText : api.appendHTML)(getValue(value));
    }
    userSet(value);
  }

  function isText() {
    return isInput(attachment);
  }

  function visible() {
    return container.classList.contains('sey-show');
  }

  function hidden(li) {
    return li.classList.contains('sey-hide');
  }

  function show() {
    if (eye) {
      eye.refresh();
    }

    setFixedPosition();

    if (!visible()) {
      container.classList.add('sey-show');
      _crossvent2.default.fabricate(attachment, 'horsey-show');
    }
  }

  function setFixedPosition() {
    if (!fixPosition || !attachment) {
      return;
    }

    requestAnimationFrame(function () {
      var _el$getBoundingClient = el.getBoundingClientRect(),
          bottom = _el$getBoundingClient.bottom,
          left = _el$getBoundingClient.left,
          right = _el$getBoundingClient.right;

      Object.assign(container.style, {
        top: bottom + 'px',
        left: left + 'px',
        width: right - left + 'px',
        maxHeight: 'calc(100vh - ' + bottom + 'px - 15px)'
      });
    });
  }

  function toggler(e) {
    var left = e.which === 1 && !e.metaKey && !e.ctrlKey;
    if (left === false) {
      return; // we only care about honest to god left-clicks
    }
    toggle();
  }

  function toggle() {
    if (!visible()) {
      show();
    } else {
      hide();
    }
  }

  function select(li) {
    unselect();

    if (li.classList.contains('sey-hide')) {
      selection = null;
      return;
    }

    if (li) {
      selection = li;
      selection.classList.add('sey-selected');
    }

    // scroll to category if first visible item
    var selectionTop = Array.from(selection.closest('.sey-list').querySelectorAll('.sey-item:not(.sey-hide)')).indexOf(selection) === 0 ? selection.closest('.sey-category').querySelector('.sey-category-id') : selection;

    // if no category
    if (!selectionTop) {
      return;
    }

    var offsetTop = selectionTop.offsetTop;
    var offsetBottom = selection.offsetTop + selection.offsetHeight;

    var visibleTop = container.scrollTop;
    var visibleBottom = visibleTop + container.offsetHeight;

    if (offsetTop < visibleTop) {
      container.scrollTop = offsetTop;
    } else if (offsetBottom > visibleBottom) {
      container.scrollTop = offsetBottom - container.offsetHeight;
    }
  }

  function unselect() {
    if (selection) {
      selection.classList.remove('sey-selected');
      selection = null;
    }
  }

  function move(up, moveCategory) {
    var total = api.source.length;
    if (total === 0) {
      return;
    }

    var first = up ? 'lastChild' : 'firstChild';
    var last = up ? 'firstChild' : 'lastChild';
    var next = up ? 'previousSibling' : 'nextSibling';

    var liIterator = selection || findList(categories[first])[first];
    var li = liIterator;

    var categoryLast = void 0;
    if (moveCategory) {
      var visibleSiblings = Array.from(liIterator.closest('.sey-list').querySelectorAll('.sey-item:not(.sey-hide)'));

      categoryLast = findCategory(li);
      // last of categoryLast, jump to next
      if (visibleSiblings.indexOf(li) === (up ? 0 : visibleSiblings.length - 1)) {
        var categoryIterator = categoryLast;
        while (categoryIterator[next]) {
          categoryIterator = categoryIterator[next];

          if (!hidden(categoryIterator)) {
            categoryLast = categoryIterator;
            break;
          }
        }
      }
    } else {
      categoryLast = categories[last];
    }

    var liLast = findList(categoryLast)[last];
    while (liIterator !== liLast) {
      var _next = findNext(liIterator);
      if (typeof up === 'undefined' && liIterator === li) {
        up = false;
        _next = liIterator;
      }

      if (!_next) {
        break;
      }

      liIterator = _next;
      if (hidden(liIterator)) {
        continue;
      }

      li = liIterator;
      if (!moveCategory) {
        break;
      }
    }

    select(li || liIterator);

    function findCategory(el) {
      while (el) {
        if (!el.parentElement) {
          console.log(el);
        }
        if (el.parentElement.classList.contains('sey-category')) {
          return el.parentElement;
        }
        el = el.parentElement;
      }
      return null;
    }

    function findNext(li) {
      if (!li) {
        return;
      }

      if (li[next]) {
        return li[next];
      }

      var category = findCategory(liIterator);
      if (category[next] && findList(category[next])[first]) {
        return findList(category[next])[first];
      }
    }
  }

  function hide() {
    if (eye) {
      eye.sleep();
    }
    container.classList.remove('sey-show');
    unselect();
    _crossvent2.default.fabricate(attachment, 'horsey-hide');
    if (el.value === lastPrefix) {
      el.value = '';
    }
  }

  function keydown(e) {
    var shown = visible();
    var which = e.which || e.keyCode;
    if (which === KEY_DOWN) {
      if (anyInput && o.autoShowOnUpDown) {
        show();
      }
      if (shown) {
        move(false, e.shiftKey);
        stop(e);
      }
    } else if (which === KEY_UP) {
      if (anyInput && o.autoShowOnUpDown) {
        show();
      }
      if (shown) {
        move(true, e.shiftKey);
        stop(e);
      }
    } else if (which === KEY_RIGHT || which === KEY_LEFT) {
      while (selection && selection.classList.contains('sey-sub-item')) {
        selection = selection.previousElementSibling;
      }

      var $submenu = selection && selection.querySelector('.sey-submenu-icon');
      if ($submenu) {
        _crossvent2.default.fabricate($submenu, 'click');
      }
    } else if (which === KEY_BACKSPACE) {
      if (anyInput && o.autoShowOnUpDown) {
        show();
      }
    } else if (shown) {
      if (which === KEY_ENTER) {
        if (selection) {
          _crossvent2.default.fabricate(selection, 'click');
        } else {
          hide();
        }
        stop(e);
      } else if (which === KEY_ESC) {
        hide();
        stop(e);
      }
    }
  }

  function stop(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  function showNoResults() {
    if (noneMatch) {
      noneMatch.classList.remove('sey-hide');
    }
  }

  function hideNoResults() {
    if (noneMatch) {
      noneMatch.classList.add('sey-hide');
    }
  }

  function filtering() {
    if (!visible()) {
      return;
    }
    debouncedLoading(true);
    _crossvent2.default.fabricate(attachment, 'horsey-filter');
    var value = readInput();
    if (!o.blankSearch && !value || state.additionalAttributes !== getAdditionalCacheAttributes()) {
      hide();
      return;
    }
    var nomatch = noMatches({ query: value });
    var count = walkCategories();
    if (count === 0 && nomatch && hasItems) {
      showNoResults();
    } else {
      hideNoResults();
    }
    if (!selection) {
      move();
    }
    if (!selection && !nomatch) {
      hide();
    }

    function walkCategories() {
      return Array.from(categories.childNodes).reduce(function (count, category) {
        var list = findList(category);
        var partial = walkCategory(list);
        category.classList[partial ? 'remove' : 'add']('sey-hide');

        count += partial;
        return count;
      }, 0);
    }

    function walkCategory(ul) {
      _crossvent2.default.fabricate(ul, 'horsey-filter');
      return ul.querySelectorAll('li:not(.sey-hide)').length;
    }
  }

  function deferredFilteringNoEnter(e) {
    var which = e.which || e.keyCode;
    if (which === KEY_ENTER) {
      return;
    }
    deferredFiltering();
  }

  function deferredShow(e) {
    var which = e.which || e.keyCode;
    if (which === KEY_ENTER || which === KEY_TAB) {
      return;
    }
    setTimeout(show, 0);
  }

  function autocompleteEventTarget(e) {
    var target = e.target;
    if (target === attachment) {
      return true;
    }
    while (target) {
      if (target === container || target === attachment) {
        return true;
      }
      target = target.parentNode;
    }
  }

  function hideOnBlur(e) {
    var which = e.which || e.keyCode;
    if (which === KEY_TAB) {
      hide();
    }
  }

  function hideOnClick(e) {
    if (autocompleteEventTarget(e)) {
      return;
    }
    hide();
  }

  function inputEvents(remove) {
    var op = remove ? 'remove' : 'add';
    if (eye) {
      eye.destroy();
      eye = null;
    }
    if (trackPosition && !remove) {
      eye = (0, _bullseye2.default)(container, attachment, {
        caret: anyInput && attachment.tagName !== 'INPUT',
        context: o.appendTo
      });
      if (!visible()) {
        eye.sleep();
      }
    }
    if (remove || anyInput && doc.activeElement !== attachment) {
      _crossvent2.default[op](attachment, 'focus', loading);
    } else {
      loading();
    }
    if (anyInput) {
      _crossvent2.default[op](attachment, 'input', deferredShow);
      _crossvent2.default[op](attachment, 'input', deferredFiltering);
      _crossvent2.default[op](attachment, 'input', deferredFilteringNoEnter);
      /*crossvent[op](attachment, 'paste', deferredFiltering);*/
      _crossvent2.default[op](attachment, 'keydown', keydown);
      if (o.autoHideOnBlur) {
        _crossvent2.default[op](attachment, 'input', hideOnBlur);
      }
    } else {
      _crossvent2.default[op](attachment, 'click', toggler);
      _crossvent2.default[op](docElement, 'keydown', keydown);
    }
    if (o.autoHideOnClick) {
      _crossvent2.default[op](doc, 'click', hideOnClick);
    }
    if (fixPosition) {
      window.addEventListener('resize', setFixedPosition);
    }
    if (form) {
      _crossvent2.default[op](form, 'submit', hide);
    }
  }

  function destroy() {
    inputEvents(true);
    if (parent.contains(container)) {
      parent.removeChild(container);
    }
  }

  function defaultSetter(value) {
    if (textInput) {
      if (setAppends === true) {
        el.value += ' ' + value;
      } else {
        el.value = value;
      }
    } else {
      if (setAppends === true) {
        el.innerHTML += ' ' + value;
      } else {
        el.innerHTML = value;
      }
    }

    el.dispatchEvent(new Event('change'));
    el.dispatchEvent(new Event('input'));
  }

  function defaultItemRenderer(li, suggestion) {
    text(li, getText(suggestion));
  }

  function defaultOverflowRenderer(li, count) {
    text(li, (i18n.more || '... ##count## more').replace(/##count##/g, count));
  }

  function defaultCategoryRenderer(div, data) {
    if (data.id !== 'default') {
      var id = tag('div', 'sey-category-id');
      div.appendChild(id);
      text(id, data.id);
    }
  }

  function loopbackToAnchor(text, p) {
    var result = '';
    var anchored = false;
    var start = p.start;
    while (anchored === false && start >= 0) {
      result = text.substr(start - 1, p.start - start + 1);
      anchored = ranchorleft.test(result);
      start--;
    }
    return {
      text: anchored ? result : null,
      start: start
    };
  }

  function filterAnchoredText(q, suggestion) {
    var position = (0, _sell2.default)(el);
    var input = loopbackToAnchor(q, position).text;
    if (input) {
      return { input: input, suggestion: suggestion };
    }
  }

  function appendText(value) {
    var current = el.value;
    var position = (0, _sell2.default)(el);
    var input = loopbackToAnchor(current, position);
    var left = current.substr(0, input.start);
    var right = current.substr(input.start + input.text.length + (position.end - position.start));
    var before = left + value + ' ';

    el.value = before + right;
    (0, _sell2.default)(el, { start: before.length, end: before.length });
  }

  function filterAnchoredHTML() {
    throw new Error('Anchoring in editable elements is disabled by default.');
  }

  function appendHTML() {
    throw new Error('Anchoring in editable elements is disabled by default.');
  }

  function findList(category) {
    return category.querySelector('.sey-list');
  }
}

function isInput(el) {
  return el.tagName === 'INPUT' || el.tagName === 'TEXTAREA';
}

function tag(type, className) {
  var el = doc.createElement(type);
  el.className = className;
  return el;
}

function defer(fn) {
  return function () {
    setTimeout(fn, 0);
  };
}

function text(el, value) {
  el.innerText = el.textContent = value;
}

function isEditable(el) {
  var value = el.getAttribute('contentEditable');
  if (value === 'false') {
    return false;
  }
  if (value === 'true') {
    return true;
  }
  if (el.parentElement) {
    return isEditable(el.parentElement);
  }
  return false;
}

function polyfills() {
  //from: https://github.com/jserz/js_piece/blob/master/DOM/ChildNode/after()/after().md
  (function (arr) {
    arr.forEach(function (item) {
      if (item.hasOwnProperty('after')) {
        return;
      }
      Object.defineProperty(item, 'after', {
        configurable: true,
        enumerable: true,
        writable: true,
        value: function after() {
          var argArr = Array.prototype.slice.call(arguments),
              docFrag = document.createDocumentFragment();

          argArr.forEach(function (argItem) {
            var isNode = argItem instanceof Node;
            docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
          });

          this.parentNode.insertBefore(docFrag, this.nextSibling);
        }
      });
    });
  })([Element.prototype /*, CharacterData.prototype, DocumentType.prototype*/]);
}
